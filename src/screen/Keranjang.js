import {
  View,
  Text,
  ScrollView,
  TextInput,
  Image,
  TouchableOpacity,
} from 'react-native';
import React from 'react';

const Keranjang = ({navigation}) => {
  return (
    <View
      style={{
        flex: 1,
        backgroundColor: '#F6F8FF',
        alignItems: 'center',
        width: '100%',
      }}>
      <ScrollView>
        <View
          style={{
            marginTop: 20,
            width: 353,
            height: 135,
            borderRadius: 8,
            backgroundColor: '#FFFFFF',
            flexDirection: 'row',
            alignItems: 'center',
          }}>
          <Image
            style={{margin: 20}}
            source={require('../assets/Sepatuu.png')}
          />
          <View>
            <Text style={{marginBottom: 10, fontWeight: 'bold'}}>
              New Balance - Pink Abu - 40
            </Text>
            <Text style={{marginBottom: 10}}>Cuci Sepatu</Text>
            <Text>Note : -</Text>
          </View>
        </View>
        <View
          style={{
            flexDirection: 'row',
            marginTop: 30,
            justifyContent: 'center',
          }}>
          <View
            style={{
              width: 20,
              height: 20,
              borderWidth: 1,
              borderColor: '#BB2427',
              borderRadius: 5,
            }}>
            <Image
              style={{width: 15, height: 15, margin: 1.5}}
              source={require('../assets/plus.png')}
            />
          </View>
          <Text style={{color: '#BB2427', marginLeft: 10}}>Tambah Barang</Text>
        </View>
      </ScrollView>
      <TouchableOpacity
        onPress={() => navigation.navigate('Summary')}
        style={{
          backgroundColor: '#BB2427',
          width: 355,
          height: 45,
          position: 'absolute',
          bottom: 40,
          justifyContent: 'center',
          alignItems: 'center',
          borderRadius: 9,
        }}>
        <Text style={{color: 'white', fontSize: 16, fontWeight: '700'}}>
          Selanjutnya
        </Text>
      </TouchableOpacity>
    </View>
  );
};

export default Keranjang;
