import {View, Text, Image, TextInput, TouchableOpacity} from 'react-native';
import React from 'react';

const Setting = ({navigation}) => {
  return (
    <View
      style={{
        flex: 1,
        backgroundColor: '#FFFFFF',
        alignItems: 'center',
        width: '100%',
      }}>
      <View style={{alignItems:'center'}}>
        <Image style={{marginTop:20}} source={require('../assets/fotoprofile.png')}/>
        <Text style={{ marginTop:10,fontSize:22,fontWeight:'bold', color:'#3A4BE0'}}>Edit Profile</Text>
      </View>
      <Text style={{marginLeft:25,marginTop:15 , color:'#BB2427',alignSelf:'flex-start'}}> Nama</Text>
        <View
        style={{
          width: 355,
          height: 45,
          backgroundColor: '#F6F8FF',
          marginLeft:20,
          marginTop: 5,
          
        }}>
        <TextInput style={{}} placeholder="Agil Bani" />
      </View>
      <Text style={{marginLeft:25,marginTop:15 , color:'#BB2427',alignSelf:'flex-start'}}> Email</Text>
        <View
        style={{
          width: 355,
          height: 45,
          backgroundColor: '#F6F8FF',
          marginLeft:20,
          marginTop: 5,
          
        }}>
        <TextInput style={{}} placeholder="gilagil@gmail.com" />
      </View>
      <Text style={{marginLeft:25,marginTop:15 , color:'#BB2427',alignSelf:'flex-start'}}> No HP</Text>
        <View
        style={{
          width: 355,
          height: 45,
          backgroundColor: '#F6F8FF',
          marginLeft:20,
          marginTop: 5,
          
        }}>
        <TextInput style={{}} placeholder="08124564879" />
      </View>
      <TouchableOpacity
        onPress={() => navigation.navigate('About')}
        style={{
          backgroundColor: '#BB2427',
          width: 355,
          height: 45,
          position: 'absolute',
          bottom: 40,
          justifyContent: 'center',
          alignItems: 'center',
          borderRadius: 9,
        }}>
        <Text style={{color: 'white', fontSize: 16, fontWeight: '700'}}>
          Simpan
        </Text>
      </TouchableOpacity>
    </View>
  );
};

export default Setting;
