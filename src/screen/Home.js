import {View, Text, Image, ScrollView, TouchableOpacity} from 'react-native';
import React from 'react';

const Home = ({navigation}) => {
  return (
    <View style={{flex: 1, backgroundColor: '#F6F8FF'}}>
      <ScrollView>
        <View style={{width: '100%', height: 270, backgroundColor: '#FFFFFF'}}>
          <View
            style={{
              flexDirection: 'row',
              alignSelf: 'center',
              marginTop: 40,
              justifyContent: 'center',
              flexDirection: 'row',
            }}>
            <Image
              style={{width: 45, height: 45}}
              source={require('../assets/Rectangle-3.png')}
            />
            <Image
              style={{marginLeft: 300}}
              source={require('../assets/Bag.png')}
            />
          </View>
          <Text style={{color: '#0A0827', marginTop: 10, marginLeft: 8}}>
            {' '}
            hallo, Agil!!
          </Text>
          <Text
            style={{
              fontSize: 20,
              fontWeight: '700',
              color: '#0A0827',
              marginTop: 10,
              marginLeft: 8,
            }}>
            {' '}
            Ingin merawat dan memperbaiki
          </Text>
          <Text
            style={{
              fontSize: 20,
              fontWeight: '700',
              color: '#0A0827',
              marginTop: 0,
              marginLeft: 8,
            }}>
            {' '}
            sepatumu? Cari disini
          </Text>
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <View
              style={{
                marginTop: 20,
                width: 275,
                height: 45,
                backgroundColor: '#F6F8FF',
                borderRadius: 14,
                justifyContent: 'center',
              }}>
              <Image
                style={{marginLeft: 8}}
                source={require('../assets/Search.png')}
              />
            </View>
            <View
              style={{
                marginTop: 20,
                marginLeft: 40,
                width: 50,
                height: 45,
                backgroundColor: '#F6F8FF',
                borderRadius: 14,
                justifyContent: 'center',
              }}>
              <Image
                style={{alignSelf: 'center'}}
                source={require('../assets/Filter.png')}
              />
            </View>
          </View>
        </View>
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <View
            style={{
              width: 95,
              height: 95,
              backgroundColor: '#FFFFFF',
              marginTop: 20,
              borderRadius: 14,
              justifyContent: 'center',
            }}>
            <Image
              style={{alignSelf: 'center', width: 60, height: 60}}
              source={require('../assets/Group17.png')}
            />
          </View>
          <View
            style={{
              width: 95,
              height: 95,
              backgroundColor: '#FFFFFF',
              marginTop: 20,
              borderRadius: 14,
              justifyContent: 'center',
              marginLeft: 27,
              marginRight: 27,
            }}>
            <Image
              style={{alignSelf: 'center', width: 60, height: 60}}
              source={require('../assets/Group17.png')}
            />
          </View>

          <View
            style={{
              width: 95,
              height: 95,
              backgroundColor: '#FFFFFF',
              marginTop: 20,
              borderRadius: 14,
              justifyContent: 'center',
            }}>
            <Image
              style={{alignSelf: 'center', width: 60, height: 60}}
              source={require('../assets/Group17.png')}
            />
          </View>
        </View>
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'center',
            alignItems: 'center',
            marginTop: 15,
            fontWeight: '700',
          }}>
          <View style={{marginRight: 130}}>
            <Text style={{fontWeight: '700'}}>Rekomensasi Terdekat</Text>
          </View>
          <View>
            <Text>View All</Text>
          </View>
        </View>
        <TouchableOpacity>

        
        <View
          style={{
            alignSelf:'center',
            borderRadius:5,
            justifyContent: 'center',
            alignItems: 'center',
            marginTop: 15,
            width:355,
            height: 135,
            backgroundColor:'#FFFFFF',
            flexDirection:'row'
          }}>
            <View style={{marginRight:15}}>
              <Image source={require('../assets/hitamputih.png')}/>
            </View>
            <View style={{}}>
              <Image source={require('../assets/bintangbintang.png')}/>
              <Text style={{marginTop:5}}>4.8 Rating</Text>
              <Text style={{marginTop:10, color:'black'}}>Jack Repair Gejayan</Text>
              <Text style={{marginTop:5,fontSize:10}}>Jl. Gejayan III No.2, Karangasem, Kec. Laweyan . . .</Text>
              <View style={{borderRadius:30,marginTop:10,backgroundColor:'#E64C3C33', width:58,height:21, alignItems:'center'}}><Text style={{color:'#EA3D3D'}}>TUTUP</Text></View>
            </View>
            <View style={{marginBottom:100}}>
            <Image source={require('../assets/lmerah.png')}/>
            </View>
        </View>
        </TouchableOpacity>
        <TouchableOpacity onPress={()=> navigation.navigate('Detail')}>
        <View
          style={{
            alignSelf:'center',
            borderRadius:5,
            justifyContent: 'center',
            alignItems: 'center',
            marginTop: 15,
            width:355,
            height: 135,
            backgroundColor:'#FFFFFF',
            flexDirection:'row'
          }}>
            <View style={{marginRight:15}}>
              <Image source={require('../assets/berwarna.png')}/>
            </View>
            <View style={{}}>
              <Image source={require('../assets/bintangbintang.png')}/>
              <Text style={{marginTop:5}}>4.7 Rating</Text>
              <Text style={{marginTop:10, color:'black'}}>Jack Repair Gejayan</Text>
              <Text style={{marginTop:5,fontSize:10}}>Jl. Gejayan III No.2, Karangasem, Kec. Laweyan . . .</Text>
              <View style={{borderRadius:30,marginTop:10,backgroundColor:'#11A84E1F', width:58,height:21, alignItems:'center'}}><Text style={{color:'#11A84E'}}>BUKA</Text></View>
            </View>
            <View style={{marginBottom:100}}>
            <Image source={require('../assets/lputih.png')}/>
            </View>
        </View>
        </TouchableOpacity>
        <View
          style={{
            alignSelf:'center',
            borderRadius:5,
            justifyContent: 'center',
            alignItems: 'center',
            marginTop: 15,
            width:355,
            height: 135,
            backgroundColor:'#FFFFFF',
            flexDirection:'row'
          }}>
            <View style={{marginRight:15}}>
              <Image source={require('../assets/hitamputih.png')}/>
            </View>
            <View style={{}}>
              <Image source={require('../assets/bintangbintang.png')}/>
              <Text style={{marginTop:5}}>4.8 Rating</Text>
              <Text style={{marginTop:10, color:'black'}}>Jack Repair Gejayan</Text>
              <Text style={{marginTop:5,fontSize:10}}>Jl. Gejayan III No.2, Karangasem, Kec. Laweyan . . .</Text>
              <View style={{borderRadius:30,marginTop:10,backgroundColor:'#E64C3C33', width:58,height:21, alignItems:'center'}}><Text style={{color:'#EA3D3D'}}>TUTUP</Text></View>
            </View>
            <View style={{marginBottom:100}}>
            <Image source={require('../assets/lmerah.png')}/>
            </View>
        </View>
        
      </ScrollView>
    </View>
  );
};

export default Home;
