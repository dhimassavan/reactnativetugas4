import {View, Text, Image, TouchableOpacity,ScrollView} from 'react-native';
import React from 'react';

const Checkout = ({navigation}) => {
  return (
    <View
      style={{
        flex: 1,
        backgroundColor: '#F6F8FF',
        alignItems: 'center',
        width: '100%',
      }}>
        <ScrollView>

        
      <View
        style={{
          marginTop: 5,
          width: '100%',
          height: 135,
          borderRadius: 8,
          backgroundColor: '#FFFFFF',
        }}>
        <View style={{margin:20,}}>
          <Text style={{marginBottom: 5}}>Data Customer</Text>
          <Text style={{fontWeight: 'bold',marginBottom: 3 ,color:'#201F26'}}>
          Agil Bani (0813763476)
          </Text>
          <Text style={{fontWeight: 'bold',marginBottom: 3,color:'#201F26'}}>
          Jl. Perumnas, Condong catur, Sleman, Yogyakarta
          </Text>
          <Text style={{fontWeight: 'bold',color:'#201F26'}}>
          gantengdoang@dipanggang.com
          </Text>
        </View>
      </View>
      <View
        style={{
          marginTop: 5,
          width: '100%',
          height: 'auto',
          borderRadius: 8,
          backgroundColor: '#FFFFFF',
        }}>
        <View style={{margin:20,}}>
          <Text style={{marginBottom: 5}}>Alamat Outlet Tujuan</Text>
          <Text style={{fontWeight: 'bold',marginBottom: 3 ,color:'#201F26'}}>
          Jack Repair - Seturan (027-343457)
          </Text>
          <Text style={{fontWeight: 'bold',marginBottom: 3,color:'#201F26'}}>
          Jl. Affandi No 18, Sleman, Yogyakarta
          </Text>
          
        </View>
      </View>
      <View
        style={{
          marginTop: 5,
          width: '100%',
          height: 'auto',
          borderRadius: 8,
          backgroundColor: '#FFFFFF',
          
        }}>
            <Text style={{marginLeft:20, marginTop:5, marginBottom:-10}}>Barang</Text>
        <View style={{ flexDirection: 'row',alignItems: 'center',}}>
        <Image style={{margin: 20}} source={require('../assets/Sepatuu.png')} />
        <View>
          <Text style={{marginBottom: 10, fontWeight: 'bold',color:'black'}}>
            New Balance - Pink Abu - 40
          </Text>
          <Text style={{marginBottom: 10}}>Cuci Sepatu</Text>
          <Text>Note : -</Text>
        </View>
      </View>
      <View style={{justifyContent:'space-around',flexDirection:'row',marginTop:-10}}>
        <Text>1 Pasang</Text>
        <Text style={{marginLeft:170,color:'black'}}>RP @50.000</Text>
      </View>
    </View>
    <View
      style={{
        marginTop: 5,
        width: '100%',
        height: 'auto',
        borderRadius: 8,
        backgroundColor: '#FFFFFF',
      }}>
      <View style={{margin:20}}>
        <Text style={{marginBottom: 5, marginTop:-10}}>Rincian Pembayaran</Text>
        <View style={{flexDirection:'row', justifyContent:'space-between'}}>
          <Text>Cuci Sepatu</Text>
          <Text>X1 Pasang</Text>
          <Text>RP 50.000</Text>
        </View>
        <View style={{flexDirection:'row', justifyContent:'space-between'}}>
          <Text>Biaya Antar</Text>
          <Text>RP 3.000</Text>
        </View>
          <View style={{borderWidth:0.2}}></View>
        <View style={{flexDirection:'row', justifyContent:'space-between'}}>
          <Text>Total</Text>
          <Text>RP 53.000</Text>
        </View>
        
        {/* <Text style={{fontWeight: 'bold',marginBottom: 3 ,color:'#201F26'}}>
        Jack Repair - Seturan (027-343457)
        </Text>
        <Text style={{fontWeight: 'bold',marginBottom: 3,color:'#201F26'}}>
        Jl. Affandi No 18, Sleman, Yogyakarta
        </Text> */}
        
      </View>
    </View>
    <View
        style={{
          marginTop: 5,
          width: '100%',
          height: 135,
          borderRadius: 8,
          backgroundColor: '#FFFFFF',
        }}>
        <View style={{margin:20,}}>
          <Text style={{marginBottom: 5}}>Data Customer</Text>
          <Text style={{fontWeight: 'bold',marginBottom: 3 ,color:'#201F26'}}>
          Agil Bani (0813763476)
          </Text>
          <Text style={{fontWeight: 'bold',marginBottom: 3,color:'#201F26'}}>
          Jl. Perumnas, Condong catur, Sleman, Yogyakarta
          </Text>
          <Text style={{fontWeight: 'bold',color:'#201F26'}}>
          gantengdoang@dipanggang.com
          </Text>
        </View>
      </View>
    <TouchableOpacity onPress={()=> navigation.navigate('Transaksi')} style={{backgroundColor:'#BB2427',width: 355,
          height: 45,
          marginTop:20,
          marginBottom:40,
          justifyContent:'center',
          alignItems:'center',
          borderRadius:9}}>
      <Text style={{color:'white',fontSize:16, fontWeight:'700'}}>
      Pesan Sekarang
      </Text>
     </TouchableOpacity>
     </ScrollView>
    </View>
  );
};

export default Checkout;
