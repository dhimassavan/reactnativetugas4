import {View, Text, Image, ImageBackground , TouchableOpacity } from 'react-native';
import React from 'react';
import Icon from 'react-native-vector-icons/FontAwesome5';

const Detail = ({navigation}) => {
  return (
    <View style={{flex: 1}}>
      <ImageBackground
        source={require('../assets/Detail.png')}
        style={{width: '100%', height: 316, alignItems:'flex-end'}}>
        <Icon name="shopping-bag" size={30} color="#fff" style={{margin:20}} /> 
      </ImageBackground>

      <View
        style={{
          backgroundColor: '#FFFF',
          width: '100%',
          height: '100%',
          marginTop: -80,
          borderRadius: 20,
        }}>
          <Text style={{margin:20,fontSize:22, fontWeight:'700', color:'#201F26'}}>
          Jack Repair Seturan
          </Text>
          <Image style={{marginLeft:20, marginTop: -10}} source={require('../assets/Bintang.png')} />
          <View style={{flexDirection:'row', marginLeft:20,marginTop:15, alignItems:'center', justifyContent:'space-between'}}>
            <Image source={require('../assets/location_outline.png')}/>
            
            <Text style={{color:'#201F26'}}>
            Jalan Affandi (Gejayan), No.15, Sleman.{'\n'}Yogyakarta, 55384
            </Text>
            
            <Text style={{marginRight:15 , color:'#3471CD'}}>
              Lihat Maps
            </Text>
          </View>
          <View style={{ borderBottomWidth: 1, borderColor: '#EEEEEE' , marginTop:10 }}>
         </View>
         <Text style={{color:'#201F26', marginLeft:20, marginTop:10, fontWeight:'700', fontSize:17}}>
         Deskripsi
         </Text>
         <Text style={{color:'#595959', marginLeft:20, marginTop:10}}>
         Lorem ipsum dolor sit amet, consectetur adipiscing elit. Massa gravida mattis arcu interdum lectus egestas scelerisque. Blandit porttitor diam viverra amet nulla sodales aliquet est. Donec enim turpis rhoncus quis integer. Ullamcorper morbi donec tristique condimentum ornare imperdiet facilisi pretium molestie.
         </Text>
         <Text style={{color:'#201F26', marginLeft:20, marginTop:10, fontWeight:'700',fontSize:17}}>
         Range Biaya
         </Text>
         <Text style={{color:'#595959', marginLeft:20, marginTop:10, fontWeight:'700',fontSize:15}}>
         Rp 20.000 - 80.000
         </Text>
         <TouchableOpacity onPress={()=> navigation.navigate('FormPemesanan')} style={{backgroundColor:'#BB2427',width: 355,
          height: 45,
          marginLeft:20,
          marginTop:15,
          justifyContent:'center',
          alignItems:'center',
          borderRadius:9}}>
      <Text style={{color:'white',fontSize:16, fontWeight:'700'}}>
        Repair Disini
      </Text>
     </TouchableOpacity>
        </View>
    </View>
  );
};

export default Detail;
