import {View, Text, Image, TouchableOpacity,ScrollView} from 'react-native';
import React from 'react';

const Summary = ({navigation}) => {
  return (
    <View
      style={{
        flex: 1,
        backgroundColor: '#F6F8FF',
        alignItems: 'center',
        width: '100%',
      }}>
        <ScrollView>

        
      <View
        style={{
          marginTop: 5,
          width: '100%',
          height: 135,
          borderRadius: 8,
          backgroundColor: '#FFFFFF',
        }}>
        <View style={{margin:20,}}>
          <Text style={{marginBottom: 5}}>Data Customer</Text>
          <Text style={{fontWeight: 'bold',marginBottom: 3 ,color:'#201F26'}}>
          Agil Bani (0813763476)
          </Text>
          <Text style={{fontWeight: 'bold',marginBottom: 3,color:'#201F26'}}>
          Jl. Perumnas, Condong catur, Sleman, Yogyakarta
          </Text>
          <Text style={{fontWeight: 'bold',color:'#201F26'}}>
          gantengdoang@dipanggang.com
          </Text>
        </View>
      </View>
      <View
        style={{
          marginTop: 5,
          width: '100%',
          height: 'auto',
          borderRadius: 8,
          backgroundColor: '#FFFFFF',
        }}>
        <View style={{margin:20,}}>
          <Text style={{marginBottom: 5}}>Alamat Outlet Tujuan</Text>
          <Text style={{fontWeight: 'bold',marginBottom: 3 ,color:'#201F26'}}>
          Jack Repair - Seturan (027-343457)
          </Text>
          <Text style={{fontWeight: 'bold',marginBottom: 3,color:'#201F26'}}>
          Jl. Affandi No 18, Sleman, Yogyakarta
          </Text>
          
        </View>
      </View>
      <View
          style={{
            marginTop: 20,
            width: 353,
            height: 135,
            borderRadius: 8,
            backgroundColor: '#FFFFFF',
            flexDirection: 'row',
            alignItems: 'center',
          }}>
          <Image
            style={{margin: 20}}
            source={require('../assets/Sepatuu.png')}
          />
          <View>
            <Text style={{marginBottom: 10, fontWeight: 'bold'}}>
              New Balance - Pink Abu - 40
            </Text>
            <Text style={{marginBottom: 10}}>Cuci Sepatu</Text>
            <Text>Note : -</Text>
          </View>
        </View>
     </ScrollView>
    <TouchableOpacity onPress={()=> navigation.navigate('Berhasil')} style={{backgroundColor:'#BB2427',width: 355,
          height: 45,
          position:'absolute',
          bottom:40,
          alignItems:'center',
          justifyContent:'center',
          borderRadius:9}}>
      <Text style={{color:'white',fontSize:16, fontWeight:'700'}}>
      Reservasi Sekarang
      </Text>
     </TouchableOpacity>
    </View>
  );
};

export default Summary;
