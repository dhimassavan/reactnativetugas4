import {View, Text} from 'react-native';
import React from 'react';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import { NavigationContainer } from '@react-navigation/native'
import Login from './screen/Login';
import Register from './screen/Register';
import Home from './screen/Home';
import MyTabs from './screen/Botnav';
import Detail from './screen/Detail';
import FormPemesanan from './screen/FormPemesanan';
import Keranjang from './screen/Keranjang';
import Summary from './screen/Summary';
import Berhasil from './screen/Berhasil';
import Transaksi from './screen/Transaksi';
import KodePesanan from './screen/KodePesanan';
import About from './screen/About';
import Setting from './screen/Setting';
import Checkout from './screen/Checkout';
import KodePromo from './screen/KodePromo';
import GunakanKode from './screen/GunakanKode';

const Stack = createNativeStackNavigator();
export default function Routing() {
  return (
    <NavigationContainer>
      <Stack.Navigator>
        {/* <Stack.Screen name="Home" component={HomeScreen}/> */}
        <Stack.Screen name="Login" component={Login}  />
        <Stack.Screen name="Register" component={Register} />
        <Stack.Screen name="Home" component={Home} options={{headerShown:false}}/>
        <Stack.Screen name="Botnav" component={MyTabs} options={{headerShown:false}}/>
        <Stack.Screen name="Detail" component={Detail}  />
        <Stack.Screen name="FormPemesanan" component={FormPemesanan}  />
        <Stack.Screen name="Keranjang" component={Keranjang}  />
        <Stack.Screen name="Summary" component={Summary}  />
        <Stack.Screen name="Berhasil" component={Berhasil}/>
        <Stack.Screen name="Transaksi" component={Transaksi} />
        <Stack.Screen name="KodePesanan" component={KodePesanan} />
        <Stack.Screen name="About" component={About} />
        <Stack.Screen name="Setting" component={Setting} />
        <Stack.Screen name="Checkout" component={Checkout} />
        <Stack.Screen name="KodePromo" component={KodePromo} />
        <Stack.Screen name="GunakanKode" component={GunakanKode} />
      </Stack.Navigator>
    </NavigationContainer>
  );
}
