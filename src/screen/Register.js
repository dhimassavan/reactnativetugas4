import {View, Text, Image, TextInput, Touchable, TouchableOpacity, ScrollView} from 'react-native';
import React from 'react';

const Register = ({navigation}) => {
  return (
    <View style={{flex: 1}}>
        <ScrollView>

      <Image
        style={{width: '100%', height: 317}}
        source={require('../assets/RectangleCopy.png')}
      />
      <View
        style={{
          width: '100%',
          height: 700,
          backgroundColor: '#FFFF',
          borderRadius: 19,
          marginTop: -180,
        }}>
          <Text style={{fontSize:24, fontWeight:'700',marginTop:15, marginLeft:20,color:'#0A0827'}}> WELLCOME,</Text>
          <Text style={{fontSize:24, fontWeight:'700',marginTop:3, marginLeft:20,color:'#0A0827'}}> Please Register</Text>
        <Text style={{marginLeft:20,marginTop:15 , color:'#BB2427'}}> Nama</Text>
        <View
        style={{
          width: 355,
          height: 45,
          backgroundColor: '#F6F8FF',
          marginLeft:20,
          marginTop: 5,
          
        }}>
        <TextInput style={{}} placeholder="Enter Your Name" />
      </View>
        <Text style={{marginLeft:20,marginTop:15 , color:'#BB2427'}}> Email</Text>
        <View
        style={{
          width: 355,
          height: 45,
          backgroundColor: '#F6F8FF',
          marginLeft:20,
          marginTop: 5,
          
        }}>
        <TextInput style={{}} placeholder="Youremail@mail.com" />
      </View>
        <Text style={{marginLeft:20,marginTop:15 , color:'#BB2427'}}> Password</Text>
        <View
        style={{
          width: 355,
          height: 45,
          backgroundColor: '#F6F8FF',
          marginLeft:20,
          marginTop: 5,
          
        }}>
        <TextInput style={{}} placeholder="Enter Password" />
      </View>
        <Text style={{marginLeft:20,marginTop:10 , color:'#BB2427'}}>Confirm Password</Text>
        <View
        style={{
          width: 355,
          height: 45,
          backgroundColor: '#F6F8FF',
          marginLeft:20,
          marginTop:5,
          
        }}>
        <TextInput style={{}} placeholder="RE-Enter Password" />
      </View>
     <View style={{flexDirection:'row',marginLeft:20,marginTop:10}}>
     <Image
        style={{marginRight:10}}
        source={require('../assets/Bitmap.png')}
        />
       <Image
        style={{marginRight:10}}
        source={require('../assets/Bitmap-1.png')}
        />
       <Image
        style={{}}
        source={require('../assets/Bitmap-2.png')}
      /> 
      
     </View>
     <TouchableOpacity style={{backgroundColor:'#BB2427',width: 355,
          height: 45,
          marginLeft:20,
          marginTop:40,
          justifyContent:'center',
          alignItems:'center',
          borderRadius:9}}>
      <Text style={{color:'white',fontSize:16, fontWeight:'700'}}>
        Register
      </Text>
     </TouchableOpacity>
     <View style={{flexDirection:'row', justifyContent:'center', alignItems:'center', marginTop:50}}>
     <Text style={{}}>
          Already have account?
        </Text>
          <TouchableOpacity onPress={()=> navigation.navigate('Login')}>
            <Text style={{color:'#BB2427'}}>Login?</Text>
          </TouchableOpacity>
     </View>
        </View>
          </ScrollView>
    </View>
  );
};

export default Register;
