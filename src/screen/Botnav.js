
import React from 'react'

import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import Home from './Home';

const Tab = createBottomTabNavigator();
import { View, Text, TouchableOpacity, Image } from 'react-native';
import { Props } from 'react-native-image-zoom-viewer/built/image-viewer.type';
import Detail from './Detail';
import Transaksi from './Transaksi';
import About from './About';

function MyTabBar({ state, descriptors, navigation }) {
  return (
    <View style={{ flexDirection: 'row',height:50, width:'100%', backgroundColor:'white', justifyContent:'center', alignItems:'center' }}>
      {state.routes.map((route, index) => {
        const { options } = descriptors[route.key];
        const label =
          options.tabBarLabel !== undefined
            ? options.tabBarLabel
            : options.title !== undefined
            ? options.title
            : route.name;

        const isFocused = state.index === index;

        const onPress = () => {
          const event = navigation.emit({
            type: 'tabPress',
            target: route.key,
            canPreventDefault: true,
          });

          if (!isFocused && !event.defaultPrevented) {
            // The `merge: true` option makes sure that the params inside the tab screen are preserved
            navigation.navigate({ name: route.name, merge: true });
          }
        };
        const labelIcon ={
            Home: 'https://w7.pngwing.com/pngs/848/762/png-transparent-computer-icons-home-house-home-angle-building-rectangle-thumbnail.png',
            Transaksi: 'https://w7.pngwing.com/pngs/327/41/png-transparent-computer-icons-financial-transaction-desktop-others-miscellaneous-service-logo-thumbnail.png',
            About: 'https://w7.pngwing.com/pngs/556/853/png-transparent-computer-icons-users-group-about-us-icon-computer-icons-users-group-about-us-thumbnail.png'
        }
        const onLongPress = () => {
          navigation.emit({
            type: 'tabLongPress',
            target: route.key,
          });
        };

        return (
          <TouchableOpacity
            accessibilityRole="button"
            accessibilityState={isFocused ? { selected: true } : {}}
            accessibilityLabel={options.tabBarAccessibilityLabel}
            testID={options.tabBarTestID}
            onPress={onPress}
            onLongPress={onLongPress}
            style={{ flex: 1 }}
          >
         
        
            <Image source={{uri:labelIcon[label]}} style={{height:20,width:20,alignSelf:'center'}} />
           
            <Text style={{ color: isFocused ? '#673ab7' : '#222', alignSelf:'center' }}>
              {label}
            </Text>
          </TouchableOpacity>
        );
      })}
    </View>
  );
}
function MyTabs() {
  return (
    <Tab.Navigator tabBar={props =><MyTabBar {...props}/> }>
      <Tab.Screen name="Home" component={Home} options={{}} />
      <Tab.Screen name="Transaksi" component={Transaksi} />
      <Tab.Screen name="About" component={About} />
    </Tab.Navigator>
  );
}
export default MyTabs