import { View, Text,TouchableOpacity, Image , ScrollView} from 'react-native'
import React from 'react'


const KodePromo = ({navigation}) => {
  return (
    <View  style={{
        flex: 1,
        backgroundColor: '#F6F8FF',
        alignItems: 'center',
        width: '100%', 
      }}>
        <View style={{width:'100%', height:79 , backgroundColor:'#FFFFFF'}}>
        <TouchableOpacity onPress={()=> navigation.navigate('GunakanKode')} style={{backgroundColor:'#C03A2B',width: 112,
          height: 45,
          marginRight:20,
          marginTop:20,
          justifyContent:'center',
          alignItems:'center',
          alignSelf:'flex-end',
          borderRadius:9,
          borderWidth:0.5,
          borderColor:'#C03A2B'
          }}>
            <Text style={{color:'white'}}> gunakan</Text>
          </TouchableOpacity>

        </View>
        <ScrollView>

        
        <TouchableOpacity onPress={()=> navigation.navigate('GunakanKode')}>
        <View
          style={{
            alignSelf:'center',
            borderRadius:5,
            justifyContent: 'center',
            alignItems: 'center',
            marginTop: 15,
            width:355,
            height: 135,
            backgroundColor:'#FFFFFF',
            flexDirection:'row'
          }}>
            <View style={{marginRight:15}}>
              <Image style={{width:80,height:80}} source={require('../assets/50.png')}/>
            </View>
            <View style={{}}>
             
              <Text style={{marginTop:10, color:'black'}}>Promo Cashback Hingga 30rb</Text>
              <Text style={{marginTop:5,fontSize:15}}>09 s/d 15 Maret 2021</Text>
              <Text style={{ marginLeft:150,marginTop:15,fontSize:15, color:'#034262'}}>Pakai Kupon</Text>
              
            </View>
    
        </View>
        </TouchableOpacity>
        <TouchableOpacity onPress={()=> navigation.navigate('GunakanKode')}>
        <View
          style={{
            alignSelf:'center',
            borderRadius:5,
            justifyContent: 'center',
            alignItems: 'center',
            marginTop: 15,
            width:355,
            height: 135,
            backgroundColor:'#FFFFFF',
            flexDirection:'row'
          }}>
            <View style={{marginRight:15}}>
              <Image style={{width:80,height:80}} source={require('../assets/50.png')}/>
            </View>
            <View style={{}}>
             
              <Text style={{marginTop:10, color:'black'}}>Promo Cashback Hingga 30rb</Text>
              <Text style={{marginTop:5,fontSize:15}}>09 s/d 15 Maret 2021</Text>
              <Text style={{ marginLeft:150,marginTop:15,fontSize:15, color:'#034262'}}>Pakai Kupon</Text>
              
            </View>
    
        </View>
        </TouchableOpacity>
        <TouchableOpacity onPress={()=> navigation.navigate('GunakanKode')}>
        <View
          style={{
            alignSelf:'center',
            borderRadius:5,
            justifyContent: 'center',
            alignItems: 'center',
            marginTop: 15,
            width:355,
            height: 135,
            backgroundColor:'#FFFFFF',
            flexDirection:'row'
          }}>
            <View style={{marginRight:15}}>
              <Image style={{width:80,height:80}} source={require('../assets/50.png')}/>
            </View>
            <View style={{}}>
             
              <Text style={{marginTop:10, color:'black'}}>Promo Cashback Hingga 30rb</Text>
              <Text style={{marginTop:5,fontSize:15}}>09 s/d 15 Maret 2021</Text>
              <Text style={{ marginLeft:150,marginTop:15,fontSize:15, color:'#034262'}}>Pakai Kupon</Text>
              
            </View>
    
        </View>
        </TouchableOpacity>
        <TouchableOpacity onPress={()=> navigation.navigate('Detail')}>
        <View
          style={{
            alignSelf:'center',
            borderRadius:5,
            justifyContent: 'center',
            alignItems: 'center',
            marginTop: 15,
            width:355,
            height: 135,
            backgroundColor:'#FFFFFF',
            flexDirection:'row'
          }}>
            <View style={{marginRight:15}}>
              <Image style={{width:80,height:80}} source={require('../assets/50.png')}/>
            </View>
            <View style={{}}>
             
              <Text style={{marginTop:10, color:'black'}}>Promo Cashback Hingga 30rb</Text>
              <Text style={{marginTop:5,fontSize:15}}>09 s/d 15 Maret 2021</Text>
              <Text style={{ marginLeft:150,marginTop:15,fontSize:15, color:'#034262'}}>Pakai Kupon</Text>
              
            </View>
    
        </View>
        </TouchableOpacity>
        <TouchableOpacity onPress={()=> navigation.navigate('Detail')}>
        <View
          style={{
            alignSelf:'center',
            borderRadius:5,
            justifyContent: 'center',
            alignItems: 'center',
            marginTop: 15,
            width:355,
            height: 135,
            backgroundColor:'#FFFFFF',
            flexDirection:'row'
          }}>
            <View style={{marginRight:15}}>
              <Image style={{width:80,height:80}} source={require('../assets/50.png')}/>
            </View>
            <View style={{}}>
             
              <Text style={{marginTop:10, color:'black'}}>Promo Cashback Hingga 30rb</Text>
              <Text style={{marginTop:5,fontSize:15}}>09 s/d 15 Maret 2021</Text>
              <Text style={{ marginLeft:150,marginTop:15,fontSize:15, color:'#034262'}}>Pakai Kupon</Text>
              
            </View>
    
        </View>
        </TouchableOpacity>
        </ScrollView>
    </View>
  )
}

export default KodePromo