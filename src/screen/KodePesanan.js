import { View, Text , Image, TouchableOpacity} from 'react-native'
import React from 'react'

const KodePesanan = ({navigation}) => {
  return (
    <View style={{
        flex: 1,
        backgroundColor: '#F6F8FF',
        alignItems: 'center',
        width: '100%'}}>
      <View style={{width:'100%', height:'auto', backgroundColor:'#FFFFFF',alignItems: 'center'}}>
      <Text style={{marginTop:10}}>20 Desember 2020  09:00</Text>
      <Text style={{marginTop:10,color:'black',fontWeight:'bold', fontSize:45}}>CS122001</Text>
      <Text style={{marginTop:10 ,fontWeight:'bold', fontSize:15}}>Sebutkan Kode Reservasi saat </Text>
      <Text style={{marginTop:10 , fontSize:15,fontWeight:'bold',marginBottom:20}}>tiba di outlet </Text>
      </View>
      <Text style={{margin:20 , alignSelf:'flex-start',color:'black',fontWeight:'bold'}}>Barang</Text>
        <Image source={require('../assets/PesananSepatu.png')}/>
      <Text style={{margin:20 , alignSelf:'flex-start',color:'black',fontWeight:'bold'}}>Status Pesanan</Text>
        <View style={{justifyContent:'space-around',alignItems:'center' ,flexDirection:'row',width:353,height:78, backgroundColor:'#FFFFFF',borderRadius:10}}>
            <View style={{width:10,height:10, borderRadius:5, backgroundColor:'#BB2427'}}></View>
            <View>
            <Text style={{color:'black',fontWeight:'bold'}}>Telah Reservasi</Text>
            <Text>20 Desember 2020</Text>
            </View>
            <Text>09.00</Text>
        </View>
        <TouchableOpacity onPress={()=> navigation.navigate('Checkout')} style={{backgroundColor:'#BB2427',width: 355,
          height: 45,
          position:'absolute',
          bottom:40,
          justifyContent:'center',
          alignItems:'center',
          borderRadius:9}}>
      <Text style={{color:'white',fontSize:16, fontWeight:'700'}}>
      Checkout
      </Text>
     </TouchableOpacity>
    </View>
  )
}

export default KodePesanan